//配置路由
var app = angular.module('myApp').config(
    function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/demo');
        $stateProvider
            .state('demo', {
                url: '/demo',
                templateUrl: './pages/demo.html',
            })
            .state('login', {
                url: '/login',
                template: '<login-component></login-component>',
            })
            .state('datas', {
                url: '/datas',
                templateUrl: './pages/datas/datas.html',
                data: {
                    title: '数据列表'
                }
            })
            .state("datas.details", {//子路由和路由参数
                url: '/details/:id',
                templateUrl: './pages/datas/details/details.html',
                controller:function($stateParams){
                    //alert($stateParams.id)
                    //$stateParams用于取得路由参数
                }
            })
            .state('servererror', {
                url: '/servererror',
                templateUrl: './pages/error_server.html',
                data: {
                    title: '服务器异常'
                }
            })
            .state('networkerror', {
                url: '/networkerror',
                templateUrl: './pages/error_network.html',
                data: {
                    title: '网络错误'
                }
            })
    }
);