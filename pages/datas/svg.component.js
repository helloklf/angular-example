var app = angular.module('myApp');
app.component('svgRate', {
    //设置模板
    templateUrl: 'pages/datas/svg.component.html',
    controllerAs: '$ctrl',
    //设置控制器
    controller: function ($timeout) {
        var $ctrl = this;
        //半径
        var radius = 20;
        //初始值：0
        $ctrl.val = '0,10000';
        //最大值
        var max = Math.floor(2 * Math.PI * radius) * 0.75;
        $ctrl.max = max + '10000';
        $ctrl.rate = 0;
        //监听值改变
        this.$onChanges = function (changes) {
            var rate = changes.rateValue.currentValue || 0;
            if (rate < 0)
                rate = 0;
            var val = max * rate + ',10000';
            $ctrl.rate = rate;
            $ctrl.val = val;

            $ctrl.itemClick = function () {
                //领取成功后的回掉
                this.receiveSuccess()
                //$ctrl.$emit('to-parent', -1 , this); 
            }
        }
    },
    //设置数据绑定
    bindings: {
        rateValue: "<",
        receiveSuccess: '&'
    }
});
