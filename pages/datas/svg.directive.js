var app = angular.module('myApp');
app.directive('svgRate2', function () {
  return {
    scope: false,
    //设置数据绑定
    bindToController: {
        rateValue: "<"
    },
    //设置模板
    templateUrl: 'pages/datas/svg.directive.html',
    //设置控制器
    controller: function ($scope, $timeout) {
      var $ctrl = this;
      //半径
      var radius = 20;
      //初始值：0
      $scope.val = '0,10000';
      //最大值
      var max = Math.floor(2 * Math.PI * radius) * 0.75;
      $scope.max = max + ',10000';
      $scope.rate = 0;
      //监听值改变
      this.$onChanges = function (changes) {
        var rate = changes.rateValue.currentValue || 0;
        if (rate < 0)
          rate = 0;
        $scope.rate = rate;
        var val = max * rate + ',10000';
        $scope.val = val;
        
        $scope.itemClick = function (item) {
          //console.log(item)
          //this.item.rate = this.item.rate - 0.05;
          $scope.$emit('receive-success', this); 
        }
      }
    }
  }
})