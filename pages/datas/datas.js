var app = angular.module('myApp');
app.controller('datasCtrl', function ($scope, $location) {
  $scope.datas = [{
    amount: '1.29',
    title: '爱奇艺 满减券',
    desc: '开通爱奇艺会员 满29元可用',
    rate: 0.5
  }, {
    amount: '3.29',
    title: '天猫 满减券',
    desc: '天猫超市 满99元可用，折扣商品不可用',
    rate: 0.2
  }];

  $scope.currentItem = {};
  $scope.$on('receive-success', function (event) {
    console.log(event, event.$index);
    $scope.datas[event.targetScope.$index].rate -= 0.05;
  });
  $scope.receiveSuccess = function (item) {
    console.log(item);
    item.rate -= 0.05;
  };
  $scope.details = function (index) {
    $scope.currentItem = $scope.datas[index];
    $location.path('/datas/details/' + index)
  };
});