var app = angular.module('myApp');
app.component('loginComponent', {
    //设置模板
    templateUrl: 'pages/login/login.html',
    controllerAs: '$ctrl',
    //设置控制器
    controller: function ($location) {
        var $ctrl = this;
        $ctrl.username = '';
        $ctrl.password = '';
        $ctrl.login = function () {
            alert('登录成功');
            $location.path('/datas');
        }
    }
})