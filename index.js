var app = angular.module('myApp', ['ui.router']);
app.controller('myCtrl', function ($scope) {
  $scope.firstName = "John";
  $scope.lastName = "Doe";
});
app.controller('myCtr2', function ($scope) {
  $scope.firstName = "John2";
  $scope.lastName = "Doe";
});
app.directive("runoobDirective", function () {
  return {
    template: "<h1>自定义指令!</h1>"
  };
});